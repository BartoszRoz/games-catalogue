package com.cdprojekt.games.authentication.dto;

import lombok.Value;

@Value
public class UserDto {
    String username;
    boolean isAdmin;
}
