package com.cdprojekt.games.authentication.config;

import com.cdprojekt.games.authentication.AuthenticationFacade;
import com.cdprojekt.games.authentication.domain.CurrentUserGetter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class AuthenticationConfiguration {
    @Bean
    CurrentUserGetter currentUserGetter() {
        return new CurrentUserGetter();
    }

    @Bean
    AuthenticationFacade authenticationFacade(CurrentUserGetter currentUserGetter){
        return new AuthenticationFacade(currentUserGetter);
    }
}
