package com.cdprojekt.games.authentication;

import com.cdprojekt.games.authentication.domain.CurrentUserGetter;
import com.cdprojekt.games.authentication.dto.UserDto;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.Optional;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@AllArgsConstructor
public class AuthenticationFacade {
    CurrentUserGetter currentUserGetter;

    public Optional<UserDto> getCurrentUser() {
        return currentUserGetter.getSignedInUser();
    }
}
