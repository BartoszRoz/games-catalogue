package com.cdprojekt.games.authentication.domain;

import com.cdprojekt.games.authentication.dto.UserDto;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

public class CurrentUserGetter {

    public Optional<UserDto> getSignedInUser() {
        SecurityContext context = SecurityContextHolder.getContext();
        if(context != null) {
            Authentication authentication = context.getAuthentication();
            if (authentication != null) {
                String loggedUserName = authentication.getName();
                boolean isAdmin = authentication.getAuthorities().stream().anyMatch(auth -> auth.getAuthority().equals("ROLE_ADMIN"));
                return Optional.of(new UserDto(loggedUserName, isAdmin));
            }
        }
        return Optional.empty();
    }
}
