package com.cdprojekt.games.shoppingcart.infrastructure.config;

import com.cdprojekt.games.authentication.AuthenticationFacade;
import com.cdprojekt.games.catalog.CatalogFacade;
import com.cdprojekt.games.purchase.PurchaseFacade;
import com.cdprojekt.games.shoppingcart.ShoppingCartFacade;
import com.cdprojekt.games.shoppingcart.domain.ShoppingCartEntryCreator;
import com.cdprojekt.games.shoppingcart.domain.ShoppingCartRepository;
import com.cdprojekt.games.shoppingcart.infrastructure.service.PaymentService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ShoppingCartConfiguration {

    @Bean
    public ShoppingCartFacade shoppingCartFacade(PurchaseFacade purchaseFacade,
                                                 ShoppingCartRepository repository,
                                                 AuthenticationFacade authenticationFacade,
                                                 CatalogFacade catalogFacade) {
        return new ShoppingCartFacade(
                new ShoppingCartEntryCreator(),
                repository,
                purchaseFacade,
                authenticationFacade,
                catalogFacade,
                new PaymentService());
    }
}
