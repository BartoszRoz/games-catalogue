package com.cdprojekt.games.shoppingcart.infrastructure.web;

import com.cdprojekt.games.shoppingcart.ShoppingCartFacade;
import com.cdprojekt.games.shoppingcart.dto.ShoppingCartDto;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@AllArgsConstructor
@RestController
@RequestMapping("/cart")
public class ShoppingCartController {
    ShoppingCartFacade shoppingCartFacade;

    @GetMapping
    ShoppingCartDto listCart() {
        return shoppingCartFacade.getShoppingCart();
    }

    @PutMapping(value = "/{title}", consumes = "application/json")
    @ResponseStatus(HttpStatus.OK)
    void addGameToCart(@PathVariable String title) {
        shoppingCartFacade.addToCart(title);
    }

    @DeleteMapping(value = "/{title}")
    @ResponseStatus(HttpStatus.OK)
    void deleteGameFromCart(@PathVariable String title) {
        shoppingCartFacade.remove(title);
    }

    @PostMapping(value = "/buy")
    @ResponseStatus(HttpStatus.CREATED)
    void buyGamesFromCart() {
        shoppingCartFacade.buy();
    }

}
