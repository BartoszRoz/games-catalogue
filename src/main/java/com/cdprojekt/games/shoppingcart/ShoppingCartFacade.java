package com.cdprojekt.games.shoppingcart;

import com.cdprojekt.games.authentication.AuthenticationFacade;
import com.cdprojekt.games.authentication.dto.UserDto;
import com.cdprojekt.games.catalog.CatalogFacade;
import com.cdprojekt.games.catalog.dto.GameDto;
import com.cdprojekt.games.purchase.PurchaseFacade;
import com.cdprojekt.games.purchase.exception.NoLoggedInUser;
import com.cdprojekt.games.shoppingcart.domain.ShoppingCartEntryCreator;
import com.cdprojekt.games.shoppingcart.domain.ShoppingCartRepository;
import com.cdprojekt.games.shoppingcart.dto.ShoppingCartDto;
import com.cdprojekt.games.shoppingcart.infrastructure.service.PaymentService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ShoppingCartFacade {
    ShoppingCartEntryCreator shoppingCartEntryCreator;
    ShoppingCartRepository cartRepository;
    PurchaseFacade purchaseFacade;
    AuthenticationFacade authenticationFacade;
    CatalogFacade catalogFacade;
    PaymentService paymentService;

    public ShoppingCartDto getShoppingCart() {
        UserDto currentUser = authenticationFacade.getCurrentUser().orElseThrow(NoLoggedInUser::new);

        return new ShoppingCartDto(getGamesOnCart(currentUser));
    }

    public void addToCart(String gameTitle) {
        UserDto currentUser = authenticationFacade.getCurrentUser().orElseThrow(NoLoggedInUser::new);
        cartRepository.save(shoppingCartEntryCreator.create(currentUser.getUsername(), gameTitle));
    }

    public void remove(String gameTitle) {
        UserDto currentUser = authenticationFacade.getCurrentUser().orElseThrow(NoLoggedInUser::new);
        cartRepository.deleteByGameTitleAndUsername(gameTitle, currentUser.getUsername());
    }

    public void buy() {
        UserDto currentUser = authenticationFacade.getCurrentUser().orElseThrow(NoLoggedInUser::new);

        List<GameDto> gamesFromCart = getGamesOnCart(currentUser);
        BigDecimal totalPrice = gamesFromCart.stream()
                .map(GameDto::getPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        boolean paymentSuccessful = paymentService.pay(totalPrice);

        if (paymentSuccessful) {
            gamesFromCart.forEach(game -> purchaseFacade.addPurchase(game.getTitle()));
            cartRepository.removeAllByUsername(currentUser.getUsername());
        }
    }

    private List<GameDto> getGamesOnCart(UserDto currentUser) {
        return cartRepository.findByUsername(currentUser.getUsername())
                .stream()
                .map(cartEntry -> catalogFacade.find(cartEntry.getGameTitle()))
                .flatMap(Optional::stream)
                .collect(Collectors.toList());
    }
}
