package com.cdprojekt.games.shoppingcart.domain;

import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.UUID;

public interface ShoppingCartRepository extends Repository<ShoppingCartEntry, UUID> {
    void save(ShoppingCartEntry shoppingCartEntry);
    List<ShoppingCartEntry> findByUsername(String username);
    void deleteByGameTitleAndUsername(String gameTitle, String username);
    void removeAllByUsername(String username);
}
