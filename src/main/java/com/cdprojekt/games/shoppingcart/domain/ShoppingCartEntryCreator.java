package com.cdprojekt.games.shoppingcart.domain;

import java.util.UUID;

public class ShoppingCartEntryCreator {
    public ShoppingCartEntry create(String username, String gameTitle) {
        return ShoppingCartEntry.builder()
                .id(UUID.randomUUID())
                .username(username)
                .gameTitle(gameTitle)
                .build();
    }
}
