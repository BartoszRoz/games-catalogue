package com.cdprojekt.games.shoppingcart.domain;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Entity
@Getter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
public class ShoppingCartEntry {
    @Id
    UUID id;
    String username;
    String gameTitle;
}
