package com.cdprojekt.games.shoppingcart.dto;

import com.cdprojekt.games.catalog.dto.GameDto;
import lombok.Value;

import java.util.List;

@Value
public class ShoppingCartDto {
    List<GameDto> gamesInCart;
}
