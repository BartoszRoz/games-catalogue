package com.cdprojekt.games.purchase;

import com.cdprojekt.games.authentication.AuthenticationFacade;
import com.cdprojekt.games.authentication.dto.UserDto;
import com.cdprojekt.games.purchase.domain.Purchase;
import com.cdprojekt.games.purchase.domain.PurchaseCreator;
import com.cdprojekt.games.purchase.domain.PurchaseRepository;
import com.cdprojekt.games.purchase.exception.NoLoggedInUser;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class PurchaseFacade {
    PurchaseCreator purchaseCreator;
    PurchaseRepository purchaseRepository;
    AuthenticationFacade authenticationFacade;

    public Page<String> getUserPurchasedGameTitles(Pageable pageable) {
        UserDto currentUser = authenticationFacade.getCurrentUser().orElseThrow(NoLoggedInUser::new);

        return purchaseRepository.findByUsername(currentUser.getUsername(), pageable)
                .map(Purchase::getGameTitle);
    }

    public void addPurchase(String title) {
        UserDto currentUser = authenticationFacade.getCurrentUser().orElseThrow(NoLoggedInUser::new);
        purchaseRepository.save(purchaseCreator.create(currentUser.getUsername(), title));
    }
}
