package com.cdprojekt.games.purchase.infrastruture.config;

import com.cdprojekt.games.authentication.AuthenticationFacade;
import com.cdprojekt.games.purchase.PurchaseFacade;
import com.cdprojekt.games.purchase.domain.PurchaseCreator;
import com.cdprojekt.games.purchase.domain.PurchaseRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PurchaseConfiguration {

    @Bean
    public PurchaseFacade purchaseFacade(PurchaseRepository purchaseRepository,
                                         AuthenticationFacade authenticationFacade) {
        return new PurchaseFacade(new PurchaseCreator(), purchaseRepository, authenticationFacade);
    }
}
