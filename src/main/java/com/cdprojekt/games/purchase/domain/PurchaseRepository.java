package com.cdprojekt.games.purchase.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.Repository;

import java.util.UUID;

public interface PurchaseRepository extends Repository<Purchase, UUID> {
    void save(Purchase purchase);
    Page<Purchase> findByUsername(String username, Pageable pageable);
}