package com.cdprojekt.games.purchase.domain;

import com.cdprojekt.games.catalog.domain.GameCreator;

import java.util.UUID;

public class PurchaseCreator extends GameCreator {
   public Purchase create(String username, String gameTitle) {
        return Purchase.builder()
                .id(UUID.randomUUID())
                .username(username)
                .gameTitle(gameTitle)
                .build();
    }
}
