package com.cdprojekt.games.catalog.infrastructure.web;

import com.cdprojekt.games.catalog.CatalogFacade;
import com.cdprojekt.games.catalog.dto.GameDraftDto;
import com.cdprojekt.games.catalog.dto.GameDto;
import com.cdprojekt.games.catalog.dto.UpdateGameRequestDto;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@AllArgsConstructor
@RestController
@RequestMapping("/game")
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class CatalogController {
    CatalogFacade catalogFacade;

    @GetMapping
    Page<GameDto> listGames(Pageable pageable) {
        return catalogFacade.listGames(pageable);
    }

    @GetMapping("/purchased")
    Page<GameDto> getPurchasedGames(Pageable pageable) {
        return catalogFacade.getPurchasedGames(pageable);
    }

    @GetMapping("/{title}")
    ResponseEntity<GameDto> search(@PathVariable String title) {
        return ResponseEntity.of(catalogFacade.find(title));
    }

    @PutMapping(value = "/{draftId}", consumes = "application/json")
    @ResponseStatus(HttpStatus.OK)
    GameDto publishDraft(@PathVariable UUID draftId) {
        return catalogFacade.publishDraft(draftId);
    }

    @PostMapping("/draft")
    @ResponseStatus(HttpStatus.CREATED)
    GameDraftDto createDraft(@RequestBody GameDto gameDto) {
        return catalogFacade.addDraft(gameDto);
    }

    @DeleteMapping("/draft/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void removeDraft(@PathVariable UUID id) {
        catalogFacade.removeDraft(id);
    }

    @PutMapping(value = "/draft/{id}", consumes = "application/json")
    @ResponseStatus(HttpStatus.OK)
    void updateDraft(@RequestBody UpdateGameRequestDto updateGameRequestDto, @PathVariable UUID id) {
        catalogFacade.updateDraft(id, updateGameRequestDto);
    }

    @GetMapping("/draft")
    Page<GameDraftDto> listDrafts(Pageable pageable) {
        return catalogFacade.listDrafts(pageable);
    }

}
