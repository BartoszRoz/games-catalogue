package com.cdprojekt.games.catalog.infrastructure.config;

import com.cdprojekt.games.catalog.CatalogFacade;
import com.cdprojekt.games.catalog.domain.GameCreator;
import com.cdprojekt.games.catalog.domain.GameDraftCreator;
import com.cdprojekt.games.catalog.domain.GameDraftRepository;
import com.cdprojekt.games.catalog.domain.GameRepository;
import com.cdprojekt.games.purchase.PurchaseFacade;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CatalogConfiguration {

    @Bean
    public CatalogFacade catalogFacade(GameRepository gameRepository, GameDraftRepository draftRepository,
                                       PurchaseFacade purchaseFacade) {
        return new CatalogFacade(new GameCreator(), new GameDraftCreator(), gameRepository, draftRepository,
                purchaseFacade);
    }
}
