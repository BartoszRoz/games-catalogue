package com.cdprojekt.games.catalog.infrastructure.web;

import com.cdprojekt.games.catalog.exception.GameDraftNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice(assignableTypes = CatalogController.class)
public class CatalogControllerExceptionHandler {

    @ExceptionHandler(GameDraftNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handleGameNotFoundException() {
    }
}
