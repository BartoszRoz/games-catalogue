package com.cdprojekt.games.catalog.domain;

import java.math.BigDecimal;
import java.util.UUID;

public class GameDraftCreator {
    public GameDraft create(String title, BigDecimal price) {
        return create(UUID.randomUUID(), title, price);
    }

    public GameDraft create(UUID draftId, String title, BigDecimal price) {
        return GameDraft.builder()
                .id(draftId)
                .title(title)
                .price(price)
                .build();
    }
}
