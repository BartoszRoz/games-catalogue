package com.cdprojekt.games.catalog.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.Repository;

import java.util.Optional;

public interface GameRepository extends Repository<Game, String> {
    Game save(Game game);
    Page<Game> findAll(Pageable pageable);
    Optional<Game> findByTitle(String title);
    void deleteByTitle(String title);
}
