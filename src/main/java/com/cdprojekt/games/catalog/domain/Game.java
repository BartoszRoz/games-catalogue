package com.cdprojekt.games.catalog.domain;

import com.cdprojekt.games.catalog.dto.GameDto;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;

@Entity
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
public class Game {

    @Id
    @Getter
    String title;
    BigDecimal price;

    public GameDto dto() {
        return new GameDto(title, price);
    }
}
