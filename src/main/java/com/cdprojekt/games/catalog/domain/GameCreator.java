package com.cdprojekt.games.catalog.domain;

import com.cdprojekt.games.catalog.dto.GameDto;

public class GameCreator {
    public Game create(GameDto gameDto) {
        return Game.builder()
                .title(gameDto.getTitle())
                .price(gameDto.getPrice())
                .build();
    }

    public Game create(GameDraft draftToPublish) {
        return Game.builder()
                .title(draftToPublish.getTitle())
                .price(draftToPublish.getPrice())
                .build();
    }
}
