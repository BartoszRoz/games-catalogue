package com.cdprojekt.games.catalog.domain;

import com.cdprojekt.games.catalog.dto.GameDraftDto;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
@Builder
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
public class GameDraft {

    @Id
    UUID id;
    String title;
    BigDecimal price;

    public GameDraftDto dto() {
        return new GameDraftDto(id, title, price);
    }
}
