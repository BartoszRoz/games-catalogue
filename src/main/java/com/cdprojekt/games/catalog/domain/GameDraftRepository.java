package com.cdprojekt.games.catalog.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.Repository;

import java.util.Optional;
import java.util.UUID;

public interface GameDraftRepository extends Repository<GameDraft, UUID> {
    GameDraft save(GameDraft gameDraft);
    Page<GameDraft> findAll(Pageable pageable);
    Optional<GameDraft> findById(UUID id);
    void deleteById(UUID id);
}
