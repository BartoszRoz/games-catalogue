package com.cdprojekt.games.catalog.exception;

import java.util.UUID;

public class GameDraftNotFoundException extends RuntimeException {
    public GameDraftNotFoundException(UUID id) {
        super(String.format("Game with id %s does not exist", id));
    }
}
