package com.cdprojekt.games.catalog.dto;

import lombok.Value;

import java.math.BigDecimal;

@Value
public class GameDto {
    String title;
    BigDecimal price;
}
