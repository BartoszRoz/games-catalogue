package com.cdprojekt.games.catalog.dto;

import lombok.Value;

import java.math.BigDecimal;
import java.util.UUID;

@Value
public class GameDraftDto {
    UUID id;
    String title;
    BigDecimal price;
}
