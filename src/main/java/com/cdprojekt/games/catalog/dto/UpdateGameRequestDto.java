package com.cdprojekt.games.catalog.dto;

import lombok.Value;

import java.math.BigDecimal;

@Value
public class UpdateGameRequestDto {

    String newTitle;
    BigDecimal newPrice;
}


