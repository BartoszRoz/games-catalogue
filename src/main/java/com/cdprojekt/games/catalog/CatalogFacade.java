package com.cdprojekt.games.catalog;

import com.cdprojekt.games.catalog.domain.*;
import com.cdprojekt.games.catalog.dto.GameDraftDto;
import com.cdprojekt.games.catalog.dto.GameDto;
import com.cdprojekt.games.catalog.dto.UpdateGameRequestDto;
import com.cdprojekt.games.catalog.exception.GameDraftNotFoundException;
import com.cdprojekt.games.purchase.PurchaseFacade;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class CatalogFacade {
    GameCreator gameCreator;
    GameDraftCreator gameDraftCreator;
    GameRepository gameRepository;
    GameDraftRepository gameDraftRepository;
    PurchaseFacade purchaseFacade;

    public List<GameDto> add(GameDto... gameDtos) {
        return Arrays.stream(gameDtos)
                .map(gameCreator::create)
                .map(gameRepository::save)
                .map(Game::dto)
                .collect(Collectors.toList());
    }

    public Page<GameDto> listGames(Pageable pageable) {
        return gameRepository.findAll(pageable)
                .map(Game::dto);
    }

    public Optional<GameDto> find(String title) {
        return gameRepository.findByTitle(title)
                .map(Game::dto);
    }

    public void removeDraft(UUID id) {
        gameDraftRepository.deleteById(id);
    }

    public void updateDraft(UUID draftId, UpdateGameRequestDto updateGameDraftDto) {
        gameDraftRepository.findById(draftId).orElseThrow(() -> new GameDraftNotFoundException(draftId));
        GameDraft updatedDraft = gameDraftCreator.create(draftId, updateGameDraftDto.getNewTitle(),
                updateGameDraftDto.getNewPrice());

        gameDraftRepository.save(updatedDraft);
    }

    public Page<GameDto> getPurchasedGames(Pageable pageRequest) {
        List<GameDto> purchasedGames = purchaseFacade.getUserPurchasedGameTitles(pageRequest)
                .stream()
                .map(gameRepository::findByTitle)
                .flatMap(Optional::stream)
                .map(Game::dto)
                .collect(Collectors.toList());

        return new PageImpl<>(purchasedGames);
    }

    public GameDraftDto addDraft(GameDto gameDto) {
        GameDraft gameDraft = gameDraftCreator.create(gameDto.getTitle(), gameDto.getPrice());
        return gameDraftRepository.save(gameDraft).dto();
    }

    public Page<GameDraftDto> listDrafts(Pageable pageable) {
        return gameDraftRepository.findAll(pageable)
                .map(GameDraft::dto);
    }

    public Optional<GameDraftDto> getDraft(UUID id) {
        return gameDraftRepository.findById(id)
                .map(GameDraft::dto);
    }

    public GameDto publishDraft(UUID id) {
        GameDraft draftToPublish = gameDraftRepository.findById(id).orElseThrow(() -> new GameDraftNotFoundException(id));
        return gameRepository.save(gameCreator.create(draftToPublish)).dto();
    }
}
