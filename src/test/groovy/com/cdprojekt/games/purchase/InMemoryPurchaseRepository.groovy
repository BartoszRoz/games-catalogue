package com.cdprojekt.games.purchase


import com.cdprojekt.games.purchase.domain.Purchase
import com.cdprojekt.games.purchase.domain.PurchaseRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable

import java.util.concurrent.ConcurrentHashMap
import java.util.stream.Collectors

class InMemoryPurchaseRepository implements PurchaseRepository {
    Map<UUID, Purchase> purchases = new ConcurrentHashMap<>()

    @Override
    void save(Purchase purchase) {
        purchases.put(purchase.getId(), purchase)
    }

    @Override
    Page<Purchase> findByUsername(String username, Pageable pageable) {
        var userPurchases = purchases.values()
                .stream()
                .filter(purchase -> purchase.getUsername().equals(username))
                .collect(Collectors.toList())

        return new PageImpl<Purchase>(new ArrayList<>(userPurchases))
    }
}
