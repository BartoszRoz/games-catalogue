package com.cdprojekt.games.purchase

import com.cdprojekt.games.authentication.AuthenticationFacade
import com.cdprojekt.games.base.SampleGames
import com.cdprojekt.games.base.SampleUsers
import com.cdprojekt.games.purchase.exception.NoLoggedInUser
import com.cdprojekt.games.purchase.infrastruture.config.PurchaseConfiguration
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import spock.lang.Specification

class PurchaseSpec extends Specification implements SampleGames, SampleUsers {
    AuthenticationFacade authenticationFacade = Stub()
    PurchaseFacade purchaseFacade = new PurchaseConfiguration()
            .purchaseFacade(new InMemoryPurchaseRepository(), authenticationFacade)

    def "should get user bought games"() {
        given: "John is logged in"
            authenticationFacade.getCurrentUser() >> Optional.of(john)
        and: "user has 2 purchased games"
            purchaseFacade.addPurchase(witcher.getTitle())
            purchaseFacade.addPurchase(cyberpunk.getTitle())
        when: "user requests games"
            Page<String> purchasedGameTitles = purchaseFacade.getUserPurchasedGameTitles(PageRequest.of(0, 2))
        then: "system returns both games"
            purchasedGameTitles.sort() == [witcher.getTitle(), cyberpunk.getTitle()].sort()
        when: "user purchases gwent"
            purchaseFacade.addPurchase(gwent.getTitle())
            purchasedGameTitles = purchaseFacade.getUserPurchasedGameTitles(PageRequest.of(0, 3))
        then: "system returns three games"
            purchasedGameTitles.sort() == [witcher.getTitle(), cyberpunk.getTitle(), gwent.getTitle()].sort()
    }

    def "should throw when user not logged in"() {
        given: "No one is logged in"
            authenticationFacade.getCurrentUser() >> Optional.empty()
        when: "user purchases gwent"
            purchaseFacade.addPurchase(gwent.getTitle())
        then: "exception is thrown"
            thrown(NoLoggedInUser)
        when: "user requests games"
            purchaseFacade.getUserPurchasedGameTitles(PageRequest.of(0, 2))
        then: "exception is thrown"
            thrown(NoLoggedInUser)
    }
}
