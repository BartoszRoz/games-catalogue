package com.cdprojekt.games.catalog


import com.cdprojekt.games.catalog.domain.GameDraft
import com.cdprojekt.games.catalog.domain.GameDraftRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable

import java.util.concurrent.ConcurrentHashMap

class InMemoryGameDraftRepository implements GameDraftRepository {
    Map<UUID, GameDraft> drafts = new ConcurrentHashMap<>();

    @Override
    GameDraft save(GameDraft gameDraft) {
        drafts.put(gameDraft.getId(), gameDraft)

        return gameDraft
    }

    @Override
    Page<GameDraft> findAll(Pageable pageable) {
        return new PageImpl<GameDraft>(new ArrayList<>(drafts.values()))
    }

    @Override
    Optional<GameDraft> findById(UUID id) {
        return Optional.ofNullable(drafts.get(id))
    }

    @Override
    void deleteById(UUID id) {
        drafts.remove(id)
    }
}