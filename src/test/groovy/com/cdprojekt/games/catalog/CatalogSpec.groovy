package com.cdprojekt.games.catalog

import com.cdprojekt.games.base.SampleGames
import com.cdprojekt.games.catalog.dto.GameDraftDto
import com.cdprojekt.games.catalog.dto.GameDto
import com.cdprojekt.games.catalog.dto.UpdateGameRequestDto
import com.cdprojekt.games.catalog.exception.GameDraftNotFoundException
import com.cdprojekt.games.catalog.infrastructure.config.CatalogConfiguration
import com.cdprojekt.games.purchase.PurchaseFacade
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import spock.lang.Specification

class CatalogSpec extends Specification implements SampleGames {
    PurchaseFacade purchaseFacade = Stub()
    CatalogFacade catalogFacade = new CatalogConfiguration().catalogFacade(new InMemoryGameRepository(),
            new InMemoryGameDraftRepository(), purchaseFacade)

    def "should add and list games"() {
        given: "catalog has two games"
            catalogFacade.add(witcher, cyberpunk)
        when: "user requests games"
            Page<GameDto> listedGames = catalogFacade.listGames(PageRequest.of(0, 2))
        then: "system returns both games"
            listedGames.sort() == [witcher, cyberpunk].sort()
    }

    def "should find game by title"() {
        given: "catalog has Witcher"
            catalogFacade.add(witcher)
        when: "user gets Witcher details"
            Optional<GameDto> returnedGame = catalogFacade.find(witcher.getTitle())
        then: "system returns Witcher"
            returnedGame.orElseThrow() == witcher
    }

    def "should not find game that is not in catalog"() {
        given: "catalog is empty"
        when: "user gets Witcher details"
            Optional<GameDto> returnedGame = catalogFacade.find(witcher.getTitle())
        then: "system returns empty optional"
            returnedGame.isEmpty()
    }

    def "should get John's bought games"() {
        given: "catalog has 3 games"
            catalogFacade.add(witcher, cyberpunk, gwent)
        and: "user has 2 purchased games"
            purchaseFacade.getUserPurchasedGameTitles(PageRequest.of(0,3)) >>
                new PageImpl<String>(List.of(witcher.getTitle(), cyberpunk.getTitle()))
        when: "user gets purchased games"
            var returnedGames = catalogFacade.getPurchasedGames(PageRequest.of(0,3))
        then: "system returns purchased games"
            returnedGames.sort() == [witcher, cyberpunk].sort()
    }

    def "should add new game draft and get it"() {
        given: "Witcher has a draft"
            GameDraftDto draft = catalogFacade.addDraft(witcher)
        when: "admin gets drafts drafts"
            Page<GameDraftDto> drafts = catalogFacade.listDrafts(PageRequest.of(0,2))
        then: "Drafts contain witcher"
            drafts.getNumberOfElements() == 1
            drafts.first().getId() == draft.getId()
            drafts.first().getTitle() == witcher.getTitle()
            drafts.first().getPrice() == witcher.getPrice()
    }

    def "should update draft price"() {
        given: "catalog has no games"
        and: "Witcher has a draft"
            GameDraftDto witcherDraft = catalogFacade.addDraft(witcher)
        when: "admin updates draft price"
            BigDecimal newPrice = new BigDecimal("35.49")
        catalogFacade.updateDraft(witcherDraft.id, new UpdateGameRequestDto(witcher.getTitle(), newPrice))
            then: "price is updated"
            var updatedWitcherDraft = catalogFacade.getDraft(witcherDraft.getId())
            updatedWitcherDraft.orElseThrow().getPrice() == newPrice
    }

    def "should throw when updating non existing draft price"() {
        given: "catalog is empty"
        when: "admin updates price of Witcher"
            BigDecimal newPrice = new BigDecimal("35.49")
            catalogFacade.updateDraft(UUID.randomUUID(), new UpdateGameRequestDto(witcher.getTitle(), newPrice))
        then: "nothing is updated"
            thrown(GameDraftNotFoundException)
            Page<GameDraftDto> listedDrafts = catalogFacade.listDrafts(PageRequest.of(0,1))
            listedDrafts.isEmpty()
    }

    def "should remove draft"() {
        given: "catalog has two drafts"
            var witherDraft = catalogFacade.addDraft(witcher)
            var cyberpunkDraft = catalogFacade.addDraft(cyberpunk)
        when: "admin removes one game"
            catalogFacade.removeDraft(witherDraft.getId())
        then: "system returns only Cyberpunk"
            Page<GameDraftDto> listedDrafts = catalogFacade.listDrafts(PageRequest.of(0, 2))
            listedDrafts.content == [cyberpunkDraft]
    }

    def "should publish draft"() {
        given: "catalog has Witcher draft and no games"
            var witcherDraft = catalogFacade.addDraft(witcher)
        when: "admin publishes witcher"
            catalogFacade.publishDraft(witcherDraft.getId())
        then: "catalog contains Witcher game"
            Page<GameDto> listedGames = catalogFacade.listGames(PageRequest.of(0, 2))
            listedGames.content == [witcher]
    }
}
