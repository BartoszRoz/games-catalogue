package com.cdprojekt.games.catalog

import com.cdprojekt.games.catalog.domain.Game
import com.cdprojekt.games.catalog.domain.GameRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable

import java.util.concurrent.ConcurrentHashMap

class InMemoryGameRepository implements GameRepository{
    Map<String, Game> games = new ConcurrentHashMap<>();

    @Override
    Game save(Game game) {
        games.put(game.getTitle(), game);

        return game;
    }

    @Override
    Page<Game> findAll(Pageable pageable) {
        return new PageImpl<Game>(new ArrayList<>(games.values()))
    }

    @Override
    Optional<Game> findByTitle(String title) {
        return Optional.ofNullable(games.get(title))
    }

    @Override
    void deleteByTitle(String title) {
        games.remove(title)
    }
}