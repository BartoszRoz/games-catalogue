package com.cdprojekt.games.catalog

import com.cdprojekt.games.base.IntegrationSpec
import com.cdprojekt.games.base.SampleGames
import com.cdprojekt.games.catalog.dto.GameDraftDto
import com.cdprojekt.games.purchase.PurchaseFacade
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders

import static org.hamcrest.Matchers.hasSize
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*

class CatalogIntegrationSpec extends IntegrationSpec implements SampleGames {
    @Autowired CatalogFacade catalogFacade
    @Autowired PurchaseFacade purchaseFacade
    @Autowired ObjectMapper objectMapper;

    @WithMockUser
    def "should list games"() {
        given: "Catalog has three games"
            catalogFacade.add(witcher, gwent, cyberpunk)
        when: "user requests games"
            ResultActions getResult = mockMvc.perform(MockMvcRequestBuilders.get("/game"))
        then: "system returns all the games"
            getResult.andExpect(status().isOk())
                .andExpect(content().json("""
                {
                    "content": [
                        { "title": "$witcher.title",    "price": $witcher.price },
                        { "title": "$gwent.title",      "price": $gwent.price  },
                        { "title": "$cyberpunk.title",  "price": $cyberpunk.price  }
                    ]
                }
                """))
        when: "user searches Cyberpunk by title"
            ResultActions searchResult = mockMvc.perform(MockMvcRequestBuilders.get("/game/$cyberpunk.title"))
        then: "system returns Cyberpunk"
            searchResult.andExpect(status().isOk())
                .andExpect(content().json("""
                {
                    "title": "$cyberpunk.title",
                    "price": $cyberpunk.price
                }
                """))
    }

    @WithMockUser
    def "should return 404 when game does not exist"() {
        when: "when user asks for a game which is not in repository"
        ResultActions getDetailsResult = mockMvc
                .perform(MockMvcRequestBuilders.get("/game/" + UUID.randomUUID().toString()))

        then: "system return 404"
        getDetailsResult.andExpect(status().isNotFound())
    }

    @WithMockUser
    def "should return paginated games list"() {
        given: "catalog has two games"
            catalogFacade.add(witcher, cyberpunk)
        when: "user requests games"
            var request = MockMvcRequestBuilders.get("/game")
                .param("page", "0")
                .param("size", "1")
            ResultActions getResult = mockMvc.perform(request)
        then: "system returns all the games"
            getResult.andExpect(status().isOk())
                    .andExpect(jsonPath("\$.content").isArray())
                    .andExpect(jsonPath("\$.content", hasSize(1)))

    }

    @WithMockUser
    def "should remove a draft"() {
        given: "catalog has two games"
            var witcherDraft = catalogFacade.addDraft(witcher)
            catalogFacade.addDraft(cyberpunk)
        when: "admin removes witcher"
            var request = MockMvcRequestBuilders.delete("/game/draft/$witcherDraft.id")
                    .with(csrf())
            ResultActions deleteResult = mockMvc.perform(request)
        then: "game is removed"
            deleteResult.andExpect(status().isNoContent())
            catalogFacade.getDraft(witcherDraft.id).isEmpty()
    }

    @WithMockUser
    def "should update a draft"() {
        given: "catalog has witcher draft"
            var witcherDraft = catalogFacade.addDraft(witcher)
            BigDecimal newPrice = new BigDecimal("100.10")
        when: "admin updates price of witcher draft"
            var request = MockMvcRequestBuilders.put("/game/draft/$witcherDraft.id")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("""
                        {
                           "newPrice": $newPrice,
                           "newTitle": "$witcher.title"
                        }
                        """)
                    .with(csrf())
            ResultActions updateResult = mockMvc.perform(request)
        then: "price is updated"
            updateResult.andExpect(status().isOk())
            catalogFacade.getDraft(witcherDraft.id).orElseThrow().getPrice() == newPrice
    }

    @WithMockUser
    def "should return 404 when updating not existing draft"() {
        given: "catalog no games"
        when: "admin updates price of witcher"
            UUID id = UUID.randomUUID()
            var request = MockMvcRequestBuilders.put("/game/draft/$id")
                .contentType(MediaType.APPLICATION_JSON)
                .content("""{"newPrice": 100}""")
                .with(csrf())
            ResultActions updateResult = mockMvc.perform(request)
        then: "price is not updated"
            updateResult.andExpect(status().isNotFound())
    }

    @WithMockUser
    def "should get purchased user games"() {
        given: "Catalog has three games"
            catalogFacade.add(witcher, gwent, cyberpunk)
        and: "User has bought 2 games"
            purchaseFacade.addPurchase(witcher.getTitle())
            purchaseFacade.addPurchase(gwent.getTitle())
        when: "user requests games"
            ResultActions getResult = mockMvc.perform(MockMvcRequestBuilders.get("/game/purchased"))
        then: "system returns all the games"
            getResult.andExpect(status().isOk())
                    .andExpect(content().json("""
                    {
                        "content": [
                            { "title": "$witcher.title",    "price": $witcher.price },
                            { "title": "$gwent.title",      "price": $gwent.price  }
                        ]
                    }
                    """))
    }

    @WithMockUser
    def "should add and list drafts"() {
        given: "Catalog has no drafts"
        when: "admin adds Witcher draft"
            var request = MockMvcRequestBuilders.post("/game/draft")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("""
                            {
                                "title": "$witcher.title",
                                "price": $witcher.price
                            }
                            """)
                    .with(csrf())
            ResultActions postResult = mockMvc.perform(request)
        then: "system returns created draft"
            var draftId = getDraftIdFromResponse(postResult)
            postResult.andExpect(status().isCreated())
                    .andExpect(content().json("""
                        {
                            "title": "$witcher.title",
                            "price": $witcher.price
                        }
                        """))
        when: "admin tries to add the same game draft twice"
            postResult = mockMvc.perform(request)
        then: "Another draft is returned"
            var draft2Id = getDraftIdFromResponse(postResult)
            postResult.andExpect(status().isCreated())
        when: "Admin gets drafts"
            ResultActions getResult = mockMvc.perform(MockMvcRequestBuilders.get("/game/draft"))
        then: "system returns both the drafts"
            getResult.andExpect(status().isOk())
                    .andExpect(content().json("""
                    {
                        "content": [
                            { "id": "$draftId", "title": "$witcher.title", "price": $witcher.price },
                            { "id": "$draft2Id", "title": "$witcher.title", "price": $witcher.price }
                        ]
                    }
                    """))
        when: "admin publishes first draft"
            request = MockMvcRequestBuilders.put("/game/$draftId")
                .contentType(MediaType.APPLICATION_JSON)
                .with(csrf())
            ResultActions putResult = mockMvc.perform(request)
        then: "draft is published"
            putResult.andExpect(status().isOk())
                    .andExpect(content().json("""
                    {
                        "title": "$witcher.title", 
                        "price": $witcher.price
                    }
                    """))
                var listedGames = catalogFacade.listGames(PageRequest.of(0,1))
                listedGames.content == [witcher]
    }

    private UUID getDraftIdFromResponse(ResultActions postResult) {
        var responseContent = postResult.andReturn().getResponse().getContentAsString()
        return objectMapper.readValue(responseContent, GameDraftDto.class).getId()
    }
}
