package com.cdprojekt.games.base

import com.cdprojekt.games.catalog.dto.GameDto
import groovy.transform.CompileStatic

@CompileStatic
trait SampleGames  {
    GameDto witcher = new GameDto("Witcher", new BigDecimal("40.15"))
    GameDto cyberpunk = new GameDto("Cyberpunk", new BigDecimal("80.99"))
    GameDto gwent = new GameDto("Gwent", new BigDecimal("0"))
}