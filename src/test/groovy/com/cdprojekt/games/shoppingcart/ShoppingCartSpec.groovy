package com.cdprojekt.games.shoppingcart

import com.cdprojekt.games.authentication.AuthenticationFacade
import com.cdprojekt.games.base.SampleGames
import com.cdprojekt.games.base.SampleUsers
import com.cdprojekt.games.catalog.CatalogFacade
import com.cdprojekt.games.purchase.PurchaseFacade
import com.cdprojekt.games.purchase.exception.NoLoggedInUser
import com.cdprojekt.games.shoppingcart.dto.ShoppingCartDto
import com.cdprojekt.games.shoppingcart.infrastructure.config.ShoppingCartConfiguration
import spock.lang.Specification

class ShoppingCartSpec extends Specification implements SampleGames, SampleUsers {
    PurchaseFacade purchaseFacade = Mock()
    AuthenticationFacade authenticationFacade = Stub()
    CatalogFacade catalogFacade = Stub()
    ShoppingCartFacade shoppingCartFacade = new ShoppingCartConfiguration()
            .shoppingCartFacade(purchaseFacade, new InMemoryShoppingCartRepository(), authenticationFacade, catalogFacade)

    def "should add and remove games from cart"() {
        given: "John is logged in"
            authenticationFacade.getCurrentUser() >> Optional.of(john)
        and: "shopping cart is empty"
        and: "catalog contains Witcher and Gwent"
            catalogFacade.find(witcher.getTitle()) >> Optional.of(witcher)
            catalogFacade.find(gwent.getTitle()) >> Optional.of(gwent)
        when: "user get shopping cart"
            ShoppingCartDto shoppingCart = shoppingCartFacade.getShoppingCart()
        then: "system returns cart with empty list"
            shoppingCart.getGamesInCart() == []
        when: "user adds Wither and Gwent to cart"
            shoppingCartFacade.addToCart(witcher.title)
            shoppingCartFacade.addToCart(gwent.title)
        then: "system returns cart with games"
            shoppingCartFacade.getShoppingCart().getGamesInCart().sort() == [witcher, gwent].sort()
        when: "user removes Gwent from the cart"
            shoppingCartFacade.remove(gwent.getTitle())
        then: "system returns cart with only one game"
            shoppingCartFacade.getShoppingCart().getGamesInCart() == [witcher]
    }

    def "should not purchase games from Cart if not logged in"() {
        given: "Nobody is logged in"
        when: "used adds Witcher to cart"
            shoppingCartFacade.addToCart(witcher.title)
        then: "exception is thrown"
            thrown(NoLoggedInUser)
    }

    def "should purchase games from Cart"() {
        given: "John is logged in"
            authenticationFacade.getCurrentUser() >> Optional.of(john)
        and: "catalog contains Witcher and Cyberpunk"
            catalogFacade.find(witcher.getTitle()) >> Optional.of(witcher)
            catalogFacade.find(cyberpunk.getTitle()) >> Optional.of(cyberpunk)
        and: "John has Witcher and Cyberpunk in Cart"
            shoppingCartFacade.addToCart(witcher.title)
            shoppingCartFacade.addToCart(cyberpunk.title)
        when: "John buys games from the cart"
            shoppingCartFacade.buy()
        then: "Items were added to purchased"
            1 * purchaseFacade.addPurchase(witcher.title)
            1 * purchaseFacade.addPurchase(cyberpunk.title)
        and: "Shopping cart was cleared"
            shoppingCartFacade.getShoppingCart().getGamesInCart().isEmpty()
    }
}
