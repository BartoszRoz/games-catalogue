package com.cdprojekt.games.shoppingcart

import com.cdprojekt.games.shoppingcart.domain.ShoppingCartEntry
import com.cdprojekt.games.shoppingcart.domain.ShoppingCartRepository

import java.util.stream.Collectors

class InMemoryShoppingCartRepository implements ShoppingCartRepository {
    Set<ShoppingCartEntry> shoppingCartEntries = new HashSet<>()

    @Override
    void save(ShoppingCartEntry shoppingCartEntry) {
        shoppingCartEntries.add(shoppingCartEntry);
    }

    @Override
    List<ShoppingCartEntry> findByUsername(String username) {
        return shoppingCartEntries.stream()
            .filter(entry -> entry.getUsername() == username)
            .collect(Collectors.toList())
    }

    @Override
    void deleteByGameTitleAndUsername(String gameTitle, String username) {
        shoppingCartEntries.stream()
            .filter(entry -> entry.getGameTitle() == gameTitle && entry.getUsername() == username)
            .findAny()
            .ifPresent(entry -> shoppingCartEntries.remove(entry))
    }

    @Override
    void removeAllByUsername(String username) {
        var cartEntries = shoppingCartEntries.stream()
                .filter(entry -> entry.getUsername() == username)
                .collect(Collectors.toList())

        cartEntries.forEach(entry -> shoppingCartEntries.remove(entry))

    }
}
