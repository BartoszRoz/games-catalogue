package com.cdprojekt.games.shoppingcart

import com.cdprojekt.games.base.IntegrationSpec
import com.cdprojekt.games.base.SampleGames
import com.cdprojekt.games.catalog.CatalogFacade
import com.cdprojekt.games.purchase.PurchaseFacade
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class ShoppingCartIntegrationSpec extends IntegrationSpec implements SampleGames {
    @Autowired ShoppingCartFacade shoppingCartFacade
    @Autowired CatalogFacade catalogFacade
    @Autowired PurchaseFacade purchaseFacade

    @WithMockUser
    def "should add, list, remove and buy games from cart"() {
        given: "Catalog has Gwent"
            catalogFacade.add(witcher, gwent)
        and: "Cart has Witcher"
            shoppingCartFacade.addToCart(witcher.title)
        when: "Adding to Cart has Gwent"
            ResultActions putResult = mockMvc.perform(MockMvcRequestBuilders.put("/cart/$gwent.title")
                    .contentType(MediaType.APPLICATION_JSON)
                    .with(csrf()))
        then: "200 is returned"
            putResult.andExpect(status().isOk())
        when: "user requests cart"
            ResultActions getResult = mockMvc.perform(MockMvcRequestBuilders.get("/cart"))
        then: "system returns games in cart"
            getResult.andExpect(status().isOk())
                .andExpect(content().json("""
                {
                    "gamesInCart": [
                        { "title": "$gwent.title",      "price": $gwent.price  },
                        { "title": "$witcher.title",    "price": $witcher.price  }
                    ]
                }
                """))
        when: "removing Gwent from the cart"
            ResultActions deleteResult = mockMvc.perform(MockMvcRequestBuilders.delete("/cart/$gwent.title")
                    .with(csrf()))
        then: "Gwent is removed"
            deleteResult.andExpect(status().isOk())
            mockMvc.perform(MockMvcRequestBuilders.get("/cart"))
                .andExpect(status().isOk())
                .andExpect(content().json("""
                {
                    "gamesInCart": [
                        { "title": "$witcher.title",    "price": $witcher.price  }
                    ]
                }
                """))
        when: "Buying items from the cart"
            ResultActions postResult = mockMvc.perform(MockMvcRequestBuilders.post("/cart/buy")
                .with(csrf()))
        then: "Witcher is purchased"
            postResult.andExpect(status().isCreated())
            mockMvc.perform(MockMvcRequestBuilders.get("/game/purchased"))
                    .andExpect(status().isOk())
                    .andExpect(content().json("""
                        {
                            "content": [
                                { "title": "$witcher.title",    "price": $witcher.price }
                            ]
                        }
                        """))

    }
}
