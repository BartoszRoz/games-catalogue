Please check the diagram explaining dependencies, modules and possible interactions.

![](GamesCatalog.drawio.png)

Tests written in Spock (Groovy) can be also a good source of documentation.

To run, you only need Java 11, the easiest would be to import project to intelliJ as Gradle project.

Database is in memory h2, so no additional setup is needed. I assumed it's not important to setup here some anything more sophisticated.

The project is a modular monolith, with the modules as on the diagram. 
Each model follows hexagonal architecture. They communicate with each other using facades

For authentication, you can use basic auth with login 'user' and password 'password1' 
There is no registration system (only hard coded users), I focused more on the business functionalities
I also disabled CSRF for simplicity to test